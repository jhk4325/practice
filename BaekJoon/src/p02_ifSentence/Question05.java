package p02_ifSentence;

import java.util.Scanner;

public class Question05 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int hour = sc.nextInt();
		int min = sc.nextInt();
		
		if(min-45<0 && hour ==0) {
				hour=23;
				int newmin = min+15;
				System.out.println(hour+" "+newmin);
		}else if(min-45<0){
			hour=hour-1;
			int newmin =min+15;
			System.out.println(hour+" "+newmin);
		}else {
			int newmin= min-45;
			System.out.println(hour+" "+newmin);
		}
	}
}
