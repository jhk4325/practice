package p05_Array;

import java.util.Arrays;
import java.util.Scanner;

public class Test04 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int[] nums = new int[10];
		int[] remains = new int[10];
		int divider= 42;
		int cnt=10;
		for(int i=0; i<nums.length;i++) {
			nums[i]=sc.nextInt();
			remains[i]=nums[i]%divider;
			for(int j=0;j<nums.length;j++) {
				if((i!=j)) {
					if(remains[i]==remains[j]) {
						cnt--;
						break;
					}
				}
			}
		}
		System.out.println(Arrays.toString(remains));
		System.out.println(cnt);
	}
}
