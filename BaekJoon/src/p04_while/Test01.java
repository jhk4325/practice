package p04_while;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Test01 {
	public static void main(String[] args) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		int a=0;
		int b=0; 
		try {
			while(true) {
			String test = br.readLine();
			String[] nums =test.split(" ");
			 a =Integer.parseInt(nums[0]);
			 b =Integer.parseInt(nums[1]);
			 if (a==0&&b==0) {
				 break;
			 }
			 bw.write(Integer.toString(a+b));
			 bw.newLine();
			}
			bw.flush();
			br.close();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
