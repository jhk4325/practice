package p04_while;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Test03 {
	public static void main(String[] args) { 
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		int n; 
		try {
			n = Integer.parseInt(br.readLine());
		int sip = 0; 
		int il = 0; 
		int tempIl = 0;  
		int cnt = 0; 
		sip = n/10; 
		il = n%10; 
		while(true) { 
			cnt++; 
			tempIl = il; 
			il = (sip + il) % 10;  
			sip = tempIl;  
			if(n == sip*10 + il) { 
				break; 
			} 
		} 
		bw.write(Integer.toString(cnt));
		bw.flush();
		br.close();
		bw.close();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}


