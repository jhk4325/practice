package p03_forSentence;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Test04 {
	public static void main(String[] args) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
		try {
			int n = Integer.parseInt(br.readLine());
			for(int i=1; i<=n; i++) {
		        String[] strs = br.readLine().split(" ");
				int a = Integer.parseInt(strs[0]);
				int b = Integer.parseInt(strs[1]);
				bw.write(Integer.toString(a+b));
				bw.newLine();
			}
		bw.flush();
		br.close();
		bw.close();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
