package p03_forSentence;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Test09 {

	public static void main(String[] args) {
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
	
	try {
		int n = Integer.parseInt(br.readLine());
		String a = "*";		
		for(int i =1; i<=n; i++) {
			for(int j =0; j<i; j++) {
			bw.write(a);
			}
			bw.newLine();
		}
		bw.flush();
		br.close();
		bw.close();
	} catch (NumberFormatException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
	
	
	
	}

}
