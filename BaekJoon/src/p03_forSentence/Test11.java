package p03_forSentence;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Test11 {

	public static void main(String[] args) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));

		
		try {
			String[] str = br.readLine().split(" ");
			int n=Integer.parseInt(str[0]);
			int x=Integer.parseInt(str[1]);
			String[] sequence = br.readLine().split(" ");
			
			for(int i=0; i<sequence.length; i++) {
				if(Integer.parseInt(sequence[i])<x) {
					bw.write(sequence[i]);
					if(i<sequence.length-1) {
					bw.write(" ");
					}
				}
			}
			bw.flush();
			br.close();
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
