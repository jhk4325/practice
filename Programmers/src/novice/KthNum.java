package novice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class KthNum {
	public static void main(String[] args) {
		int[] example= new int[] {1,5,2,6,3,7,4};
		int[][] commands= new int[][] {{2,5,3},{4,4,1},{1,7,3}};
		
		int[] ans= solution(example, commands);
	}
	
	public static int[] solution(int[]array, int[][] commands ) {
		String tmp ="";
		String[] process = {};
		int[] answer = new int[commands.length];
			for(int i= 0; i<commands.length; i++) {
				for(int j = commands[i][0]-1; j<commands[i][1]; j++) {
					if(j<commands[i][1]-1) {
					tmp+=Integer.toString(array[j])+" ";
					}else {
						tmp+=Integer.toString(array[j]);
					}
				}
				tmp+="/";
				process = tmp.split("/");
				String[] tempPc =process[i].split(" ");
				ArrayList<Integer> temp = new ArrayList<Integer>();
				for(int k =0; k<tempPc.length; k++) {
					int a=Integer.parseInt(tempPc[k]);
					temp.add(a);
				}
				temp.sort(null);
				answer[i]=temp.get(commands[i][2]-1); 
				
			}
		return answer;
	}
}

